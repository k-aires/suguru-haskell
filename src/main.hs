import Data.Function (fix)

import Board
import ManipList

-- Verifica as células vizinhas e da área e retorna os valores já preenchidos
getImpossible :: Board -> (Int,Int) -> [Int]
getImpossible b p = joinSet
    (getNeighbourFixed b p)
    (getAreaFixed b (getArea b p))

-- Pega a posição da célula de uma área que possui o valor único
markUnique :: Board -> Area -> [[Int]] -> Int -> Board
markUnique board area possible unique = do
    let pos = area !! (getPosition (head (filter (\x -> elem unique x) possible)) possible)
    let cell = Fixed unique
    updateBoard board pos cell

-- Passa por todos os uniques de uma área marcando
markAllUnique :: Board -> Area -> [[Int]] -> [Bool] -> Board
markAllUnique board area possible unique
    | not (elem True unique) = board
    | (last unique) = markAllUnique (markUnique board area possible (length unique)) area possible (init unique)
    | otherwise = markAllUnique board area possible (init unique)

-- Dado uma área, calcula as células a serem marcadas que são a única opção de um número
markUniqueArea :: Board -> Area -> Board
markUniqueArea board [] = board
markUniqueArea board area = do
    let possible = getAreaPossible board area
    let unique = map (\x -> uniqueInLists x possible) [1..(length area)]
    let fixed = getAreaFixed board area
    let newUnique = map (\x -> if (unique !! (x-1)) && not (elem x fixed) then True else False) [1..(length area)]
    markAllUnique board area possible newUnique

-- Percorre todas as áreas, marcando as células
markAllAreas :: Board -> Int -> Board
markAllAreas (Solve grid []) _ = (Solve grid [])
markAllAreas (Solve grid areas) len
    | len == 0 = markUniqueArea (Solve grid areas) (head areas)
    | otherwise = markAllAreas (markUniqueArea (Solve grid areas) (areas !! len)) (len-1)

-- Calcula uma rodada de marcações
playRound :: Board -> Board
playRound (Solve grid areas) = do
    let board = (Solve grid areas)
    let newGrid = map (\x -> map (\y -> updateCell (getCell board (x,y)) (length (getArea board (x,y))) (getImpossible board (x,y))) [1..(length (grid !! (x-1)))]) [1..(length grid)]
    let marked = map (\x -> map (\y -> markCell y) x) newGrid
    markAllAreas (Solve marked areas) ((length areas)-1)

-- Calcula os valores do tabuleiro e aplica as mudanças nas células
-- Caso existam mudanças, continua a recursão
play :: Board -> Board -> Int -> (Board,Int)
play new last count
    | new == last = (new,count)
    | otherwise = play (playRound new) new (count+1)


main = do
    board <- getBoard
    print board
    let (newBoard, count) = play (playRound board) board 1
    print count
    print newBoard
