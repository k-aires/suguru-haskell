module ManipList (removeFromList,
                  removeDuplicate,
                  joinSet,
                  uniqueInLists,
                  getPosition
                 ) where

-- Functions

-- Calcula a diferença entre duas listas, fazendo l1 - l2
removeFromList :: [Int] -> [Int] -> [Int]
removeFromList l r
    | null l = []
    | null r = l
removeFromList (h:t) r
    | not (elem h r) = h:(removeFromList t r)
    | otherwise = removeFromList t r

-- Remove elementos duplicados de uma lista
removeDuplicate :: [Int] -> [Int]
removeDuplicate [] = []
removeDuplicate (a:b)
    | elem a b = removeDuplicate b
    | otherwise = a:(removeDuplicate b)

-- Junta duas listas, sem elementos duplicados
joinSet :: [Int] -> [Int] -> [Int]
joinSet a b = removeDuplicate(a ++ b)

-- Procura se elemento é único em um conjunto de listas
uniqueInLists :: Int -> [[Int]] -> Bool
uniqueInLists e lists = if ((countInLists e lists) == 1) then True else False

-- Retorna em quantas listas um elemento aparece
countInLists :: Int -> [[Int]] -> Int
countInLists _ [] = 0
countInLists e (list:next)
    | elem e list = (countInLists e next)+1
    | otherwise = countInLists e next

-- Retorna a posição de um determinado elemento na lista
-- Retorna -1 caso não ache
getPosition :: [Int] -> [[Int]] -> Int
getPosition _ [] = -1
getPosition e (a:b)
    | not (elem e (a:b)) = -1
    | e == a = 0
    | otherwise = (getPosition e b)+1
