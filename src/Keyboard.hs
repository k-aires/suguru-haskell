module Keyboard (getInt,
                 getFloat,
                 getBool,
                 getNLines
                ) where


import ManipStr (splitStr)


-- Helpers

-- Converte uma string para Bool
strToBool :: [Char] -> Bool
strToBool "True" = True
strToBool "False" = False


-- Functions

-- Pega a próxima linha e retorna como Int
getInt :: IO Int
getInt = do
    readKb <- getLine
    let kb = (read readKb :: Int)
    return kb

-- Pega a próxima linha e retorna como Float
getFloat :: IO Float
getFloat = do
    readKb <- getLine
    let kb = (read readKb :: Float)
    return kb

-- Pega a próxima linha e retorna como Bool
getBool :: IO Bool
getBool = do
    readKb <- getLine
    let kb = strToBool readKb
    return kb

-- Pega as N próximas linhas
getNLines :: Int -> IO [[Char]]
getNLines 2 = do
    line1 <- getLine
    line2 <- getLine
    return [line1,line2]
getNLines n = do
    line <- getLine
    next <- getNLines (n-1)
    let ret = line:next
    return ret
